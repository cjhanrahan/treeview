module.exports = function(grunt) {

	function shellLog(err, stdout, stderr, cb) {
		console.log(stdout);
		cb();
	}

	grunt.initConfig({
		pkg: grunt.file.readJSON('package.json'),

		clean: {
			build: {
				src: ['build/**']
			},
		},

		copy: {
			main: {
				files: [{
					expand: true,
					cwd: 'dev/',
					src: '*.html',
					build: 'dest'
				}, {
					expand: true,
					cwd: 'dev/src/',
					src: '*.js',
					dest: 'build/src'
				}, {
					expand: true,
					cwd: 'dev/lib',
					src: '*',
					dest: 'build/lib'
				}, {
					expand: true,
					cwd: 'dev/css/fonts',
					src: '*',
					dest: 'build/css/fonts'
				}, {
					expand: true,
					cwd: 'dev/templates',
					src: '*',
					dest: 'build/templates'
				}]
			}
		},

		sass: {
			dist: {
				options: {
					style: 'expanded',
					sourcemap: true
				},
				files: [{
					expand: true,
					cwd: 'dev/css',
					src: ['*.scss'],
					dest: 'build/css/',
					ext: '.css'
				}],
			}
		},



		postcss: {
			options: {
				map: true,
				processors: [
					require('autoprefixer-core')({
						browsers: 'last 2 versions'
					}).postcss
				]
			},
			dis: {
				src: 'build/css/*.css',
			}
			// dest: 'build/css/'
		},

		shell: {
			target: {
				command: './server/start_serv.sh',
				options: {
					callback: shellLog
				}

			}
		},


	});
	grunt.loadNpmTasks('grunt-contrib-clean');
	grunt.loadNpmTasks('grunt-contrib-copy');
	grunt.loadNpmTasks('grunt-contrib-sass');
	grunt.loadNpmTasks('grunt-nginx');
	grunt.loadNpmTasks('grunt-shell');
	grunt.loadNpmTasks('grunt-postcss')
	grunt.registerTask('default', ['clean', 'copy', 'sass', 'postcss', 'shell']);

}