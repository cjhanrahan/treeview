define({
    8: 'backspace',
    9: 'tab',
    13: 'enter',
    17: 'control',
    18: 'alt',
    27: 'esc',
    37: 'arrowLeft',
    38: 'arrowUp',
    39: 'arrowRight',
    40: 'arrowDown',
    91: 'windowsKey'
});