  define(function(require) {
    var keymap = require('keymap'),
      utils = require('utils'),
      nodeHtml = require('text!../templates/node.html');

    var listbuilder = {

      keyDownHandler: function(event) {
        var app = require('app'),
          keyPressed = keymap[event.which],
          pressedWithShift = event.shiftKey,
          $thisNode = this.getCurrentNode();

        console.log('Which key', event.which, keyPressed, event);

        if (keyPressed === 'alt')
          app.changeMode();

        if (app.getMode() === 'controlMode') {
          switch (keyPressed) {
            case 'enter':
              this.addSiblingNodeOf($thisNode, pressedWithShift);
              break;

            case 'arrowUp':
              this.focusOnPrevSibling($thisNode);
              break;

            case 'arrowDown':
              this.focusOnNextSibling($thisNode);
              break;

            case 'arrowLeft':
              this.focusOnParent($thisNode);
              break;

            case 'arrowRight':
              this.focusOnFirstChild($thisNode);
              break;

            case 'tab':
              this.addChildNodeOf($thisNode);
              event.preventDefault()
              break;
          }
          event.preventDefault()
        }
      },

      inputHandler: function(event) {
        var $target = $(event.target),
          nodeNum = $target.data('node-num'),
          $textarea = this.getNodeElement(nodeNum, 'textarea'),
          $expander = this.getNodeElement(nodeNum, '.textAreaExpander');

        $expander.text($textarea.val());
      },


      currentNodeNum: 0,

      getCurrentNode: function() {
        var $textArea = this.getCurrentTextArea();
        return $textArea.parent().parent();
      },

      getCurrentTextArea: function() {
        return $(document.activeElement);
      },

      buildNode: function() {
        var lb = this,
          nodeTemplate = _.template(nodeHtml),
          node = nodeTemplate({
            nodeNum: ++lb.currentNodeNum
          });

        return $(node);
      },

      addSiblingNodeOf: function($thisNode, isPrevious) {
        var $parentList = $thisNode.parent(),
          $newNode = this.buildNode();

        if (isPrevious) {
          $newNode = $newNode.insertBefore($thisNode)
        } else {
          $newNode = $newNode.insertAfter($thisNode)
        }
        $newNode.children('textarea').focus();
      },

      addChildNodeOf: function($thisNode) {
        var $subList = $thisNode.children('ul'),
          $newNode = this.buildNode();

        $newNode
          .appendTo($subList)
          .children('textarea').focus();
      },

      focusOnPrevSibling: function($thisNode) {
        var $prevSibling = $thisNode.prev();

        this.focusOn($prevSibling);
      },

      focusOnNextSibling: function($thisNode) {
        var $nextSibling = $thisNode.next();

        this.focusOn($nextSibling);
      },

      focusOnParent: function($thisNode) {
        var $parent = $thisNode.parent().parent();

        this.focusOn($parent);
      },

      focusOnFirstChild: function($thisNode) {
        var $child = $thisNode.children('ul').children('li:first-child');

        this.focusOn($child);
      },

      focusOn: function($node) {
        var nodeNum = $node.data('node-num');
        $('textarea[data-node-num="' + nodeNum + '"]').focus();
      },

      getNodeElement: function(nodeNum, selector) {
        var nodeSelector = selector + '[data-node-num="' + nodeNum + '"]';

        return $(nodeSelector);
      }

    }

    return listbuilder;
  });