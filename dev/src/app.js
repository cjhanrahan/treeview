define(function(require) {

  var listbuilder = require('listbuilder'),
    app = {

      setUpPage: function() {
        this.debugSetup();
      },

      setInputHandler: function(inputSelector, handlerFunc) {
        var $input = $(inputSelector);
       
        $input.off('input');
        $input.on('input', function(event) {
          handlerFunc(event);
        });
      },

      setKeyDownHandler: function(targetSelector, handlerFunc) {
        var $input = $(targetSelector);

        $input.off('keydown');
        $input.on('keydown', function(event) {
          handlerFunc(event);
        });
      },


      debugSetup: function() {
        var $content = $('.content'),
          keyHandler = listbuilder.keyDownHandler.bind(listbuilder),
          inputHandler = listbuilder.inputHandler.bind(listbuilder);

        listbuilder.addChildNodeOf($content);
        this.setKeyDownHandler('body', keyHandler);
        this.setInputHandler('textarea', inputHandler);
      },

      mode: 'controlMode',

      changeMode: function() {
        var $modeElement = $('body'),
          $modeLabel = $('#modeLabel');

        if (this.mode === 'controlMode') {
          this.mode = 'editMode';
          $modeElement.removeClass('controlMode')
          $modeElement.addClass('editMode');
          $modeLabel.text("EDIT MODE");
        } else {
          this.mode = 'controlMode';
          $modeElement.removeClass('editMode');
          $modeElement.addClass('controlMode');
          $modeLabel.text("CONTROL MODE");
        }
      },

      getMode: function() {
        return this.mode;
      },


    };
  return app;
});