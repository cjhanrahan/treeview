require.config({
    baseUrl: 'build/src/',
    paths: {
        lib: '../lib',
        jquery: '../lib/jquery-2.1.1',
        underscore: '../lib/underscore',
        text: '../lib/text'
    }
});

require(['jquery', 'underscore', 'app', 'require'],
    function($, _, app, write, jquery) {
        app.setUpPage();
    }
);