define(['require', 'keymap'], function(require, keymap) {

  var write = {

    append: function(selector, text) {
      $dest = $(selector);
      $dest.append(text);
      this.checkTextProcessingSpeed(text);
    },

    backspace: function(selector) {
      var $dest = $(selector),
        currentText = $dest.text(),
        backspacedText = currentText.slice(0, currentText.length - 1);

      $dest.text(backspacedText);
    },

    checkTextProcessingSpeed: function(text) {
      if (text.length > 1) {
        console.info("More than one char in buffer", text);
      }
    }


  };

  return write;

});